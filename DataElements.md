# Data Elements

### 1. Create a data element ZRAPH_##_HOTEL_NAME

This data element should represent the name of a hotel.  
Use the domain ZRAPH_##_HOTEL_NAME as underlying type.  
Give it the label “Hotel Name” in all 4 label lengths.


### 2. Create a data element ZRAPH_##_HOTEL_ID

This data element should represent the identifier of a hotel.  
Use the fitting self-created Domain as underlying type.  
Give it the label “Hotel ID” in all 4 label lengths.


### 3. Create a data element ZRAPH_##_ROOM_RSV_ID

Use the fitting self-created Domain as underlying type.  
Give it the label “Room Reservation ID” where possible and shorten the name in other cases.


### 4. Create a data element ZRAPH_##_ROOM_RSV_PRICE

Use the fitting self-created Domain as underlying type.  
Give it the text “Room Price” for medium, long and heading labels but “Price” as the short label.


### 5. Create a data element ZRAPH_##_ROOM_TYPE

Use the fitting self-created Domain as underlying type.  
Give it the label “Room Type” for all label lengths.