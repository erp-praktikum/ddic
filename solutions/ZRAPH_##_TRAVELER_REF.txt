@EndUserText.label : 'Traveler Info'
@AbapCatalog.enhancement.category : #NOT_EXTENSIBLE
define structure zraph_##_traveler_ref {
  first_name  : /dmo/first_name;
  last_name   : /dmo/last_name;
  travel_info : reference to zraph_##_travel_info;

}